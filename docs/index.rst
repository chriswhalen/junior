it's junior!
============

junior is a full stack web framework, powered by `Flask`_.

Flask users will find junior's interface familiar, as it integrates many popular
community projects into a single workspace. New users are encouraged to explore
Flask's documentation to learn more about its core concepts, but this won't be
mandatory, as junior presents a unique interface that's described here in this
document. All of our API components are also cross-referenced directly to their
source project's documentation.

.. _Flask: https://flask.palletsprojects.com

.. toctree::
   :maxdepth: 2

   api
